#!/usr/bin/env bash

#
#	Provisioning
#
sudo apt-get update
sudo apt-get install -y python-dev
sudo apt-get install -y python-pip
sudo apt-get install -y nodejs
sudo apt-get install -y npm 
sudo apt-get install -y git
sudo ln -s /usr/bin/nodejs /usr/bin/node

#
#	Clone project repository
#
cd /home/vagrant
git clone https://hwong_earls@bitbucket.org/hwong_earls/webserver.git

#
#	Set project environment
#
cd /home/vagrant/webserver
rm -rf lib
pip install -r requirements.txt -t lib
cd /home/vagrant/webserver/html
npm install
sudo chown -R vagrant:vagrant /home/vagrant/webserver
sudo -u vagrant node_modules/.bin/bower install
